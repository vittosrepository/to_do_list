import './App.css';
import  React , {useState} from "react";
import List from './Components/List/List';
import Form from './Components/Form/Form';

function App() {

  const toDoList = [
    {id : 1 , title : 'Fare la spesa', description : 'Lorem ipsum', done : false},
    {id : 2, title : 'Passeggiata col cane', description : 'Lorem ipsum', done : true}
  ];
  const [todo, setTodo] = useState(toDoList);


  return (
    <>
      <div className='container mt-5 mb-5'>
        <div className='row'>
          <div className='col-12'>
            <h1 className='h1 text-shadow'>Ciao benvenuti a tutti sono nella todo List</h1>
          </div>
        </div>
      </div>
      
      <div className='container'>
        <div className='row'>

          <div className='col-12 col-md-6'>
          <Form todo={todo} setTodo={setTodo} />
          </div>

          <div className='col-12 col-md-6'>
            <List todo={todo} setTodo={setTodo} />
          </div>
          
        </div>
      </div>

    </>
  );
}

export default App;
