import Card from "../UI/Card/Card";

export default function List(props) {


      return(
            <>
              <h2>To Do:</h2>
              {props.todo.filter(el => !el.done).map(el => {
                  return <Card key={el.id} id={el.id} title={el.title} description={el.description} done={el.done} todo={props.todo} setTodo={props.setTodo} />
              })}
              <hr />
              <h2>Done:</h2>
              {props.todo.filter(el => el.done).map(el => {
                  return <Card key={el.id} id={el.id} title={el.title} description={el.description} done={el.done} todo={props.todo} setTodo={props.setTodo} />
              })}
            </>
      );
}

