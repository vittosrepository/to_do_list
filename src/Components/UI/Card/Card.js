export default function Card(props) {


      function setDone() {
            
            const index = props.todo.findIndex(el => el.id == props.id)
            
            let edited = props.todo.map((el,i) =>{
                  if(i == index) {
                        el.done = !el.done;
                        return el
                  } else {
                        return el
                  }
            })

            // console.log(edited);

            props.setTodo(edited)
      }

      // console.log(props);

      return(
            <div className="card my-3 bg-dark text-white">
                  <div className="card-body">
                        <h5 className="card-title">{props.title}</h5>
                        <p className="card-text">{props.description}</p>

                        <div className="form-check form-switch">
                              <input className="form-check-input"  type="checkbox" id="flexSwitchCheckDefault" defaultChecked={props.done} onChange={setDone} />
                              <label className="form-check-label" htmlFor="flexSwitchCheckDefault">Attività completata</label>
                        </div>
                  </div>
            </div>
      );
}