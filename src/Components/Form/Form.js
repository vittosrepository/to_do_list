import { useState } from "react";

export default function Form(props) {

      const [title, setTitle] = useState('');
      const [description, setDescription] = useState('');

      function handleSubmit(e) {

            e.preventDefault();

            props.setTodo(props.todo.concat( { title, description, done : false } ))

            setTitle('');
            setDescription('');
      }


      return(
            <form onSubmit={handleSubmit}>
                  <div className="mb-3">
                        <label htmlFor="exampleInputEmail1" className="form-label">Title</label>
                        <input 
                              onChange={(e) => setTitle(e.target.value)} 
                              type="text" 
                              className="form-control" 
                              id="exampleInputEmail1" 
                              aria-describedby="emailHelp" 
                              value={title} 
                        />
                        <div id="emailHelp" className="form-text">{title}</div>
                  </div>
                  <div className="mb-3">
                        <label htmlFor="exampleInputPassword1" className="form-label">Description</label>
                        <textarea 
                              onChange={(e) => setDescription(e.target.value)} 
                              className="form-control" id="exampleInputPassword1" 
                              value={description}> 
                        </textarea>
                  </div>
                  <button type="submit" className="btn btn-primary">Submit</button>
            </form>
      );
}